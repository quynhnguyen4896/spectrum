var data = {
    "id": 2,
    "title": "Some template title1",
    "prescriber": "Yes",
    "therapeutic": "Medicine",
    "ingredient": "Ingredient 9",
    "dosage_form": "Oral",
    "indication": "Indication 7",
    "notify": "No",
    "state": "State 7",
    "category": "No",
    "dosage_frequency": "1 tds",
    "duration": "1",
    "duration_unit": "Month(s)",
    "trade_name": "Some trade name",
    "sponsor": "Some supplier",
    "supply_date": "2020-03-07",
    "diagnosis": "Diagnosis description....",
    "clinical_justification": "Clinical justification description....",
    "intended_monitoring": "Some comments on monitoring....",
    "prescriber_qualification": "MBBS, M.D......",
    "created_at": "2020-03-07 14:18:16",
    "updated_at": "2020-03-07 14:31:36"
}

console.log('hello world!')

var check = function(id, val) {
    console.log('checking ', id);
    var el = document.getElementById(id);
    if (el != null) {
        el.checked = val;
    }
}

var setVal = function(id, val) {
    console.log('setVal ', id, val);
    var el = document.getElementById(id);
    if (el != null) {
        el.value = val;
    }
}

// window.document.onload = function(e){ 
    check('tgasas_hassupportinginformation_0', true);
    
    console.log(data['therapeutic'])
    switch (data['therapeutic']) {
        case 'Medicine': 
            console.log('Medicine');
            check('tgasas_therapeuticgoodtype_0', true);
            break;
        case 'Vaccine':
            console.log('Vaccine');
            check('tgasas_therapeuticgoodtype_1', true);
            break;
        case 'Biological':
            console.log('Biological');
            check('tgasas_therapeuticgoodtype_2', true);
            break;
        case 'Medical Device':
            console.log('Medical Device');
            check('tgasas_therapeuticgoodtype_3', true);
            break;
        case 'Combination':
            console.log('Combination');
            check('tgasas_therapeuticgoodtype_4', true);
            break;
    }

    // document.getElementById('tgasas_dosageregimen').value = data['dosage_frequency']
    setVal('tgasas_dosageregimen', data['dosage_frequency'])
    setVal('tgasas_durationofsupply', data['duration'])
    // setVal('tgasas_dosageregimen', data['duration_unit'])

    // document.getElementById('tgasas_durationofsupply').value = data['duration']
    // document.getElementById('tgasas_dosageregimen').value = data['duration_unit']

    switch(data['duration_unit']) {
        case 'Minute(s)':
            // document.getElementById("mySelect").value = "969230000";
            setVal('tgasas_durationofsupplyunit', '969230000')
            break;
        case 'Hour(s)':
            setVal('tgasas_durationofsupplyunit', '969230001')
            break;
        case 'Day(s)':
            setVal('tgasas_durationofsupplyunit', '969230002')
            break;
        case 'Week(s)':
            setVal('tgasas_durationofsupplyunit', '969230003')
            break;
        case 'Month(s)':
            setVal('tgasas_durationofsupplyunit', '969230004')
            break;
        case 'Year(s)':
            setVal('tgasas_durationofsupplyunit', '969230005')
            break;
    }

    setVal('tgasas_tradename', data['trade_name']);
    setVal('tgasas_tradename', data['sponsor']);
    setVal('tgasas_intendeddateofsupply', data['supply_date']);
    setVal('tgasas_diagnosis', data['diagnosis']);
    setVal('tgasas_clinicaljustification', data['clinical_justification']);
    setVal('tgasas_intendedmonitoring', data['intended_monitoring']);

// }