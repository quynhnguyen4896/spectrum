import React from 'react';
import logo from './logo.svg';
import './App.css';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
import { Stack, IStackTokens } from 'office-ui-fabric-react/lib/Stack';
import { Separator } from 'office-ui-fabric-react/lib/Separator';
import { mergeStyles } from 'office-ui-fabric-react/lib/Styling';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { getRTL } from 'office-ui-fabric-react/lib/Utilities';
import { FocusZone, FocusZoneDirection } from 'office-ui-fabric-react/lib/FocusZone';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { Image, ImageFit } from 'office-ui-fabric-react/lib/Image';
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import { List } from 'office-ui-fabric-react/lib/List';
import { ITheme, mergeStyleSets, getTheme, getFocusStyle } from 'office-ui-fabric-react/lib/Styling';
import { createListItems, IExampleItem } from '@uifabric/example-data';
import { initializeIcons } from '@uifabric/icons';
initializeIcons();

const templates = [
  {
  "id": 2,
  "title": "Template for medicine 1 with ingredient",
  "prescriber": "Yes",
  "therapeutic": "Medicine",
  "ingredient": "Ingredient 9",
  "dosage_form": "Oral",
  "indication": "Indication 7",
  "notify": "No",
  "state": "State 7",
  "category": "No",
  "dosage_frequency": "1 tds",
  "duration": "1",
  "duration_unit": "Month(s)",
  "trade_name": "Some trade name",
  "sponsor": "Some supplier",
  "supply_date": "2020-03-07",
  "diagnosis": "Diagnosis description....",
  "clinical_justification": "Clinical justification description....",
  "intended_monitoring": "Some comments on monitoring....",
  "prescriber_qualification": "MBBS, M.D......",
  "created_at": "2020-03-07 14:18:16",
  "updated_at": "2020-03-07 14:31:36"
  },
  {
  "id": 3,
  "title": "template for abc process for doctors",
  "prescriber": "No",
  "therapeutic": "Medical Device",
  "ingredient": "Ingredient 4",
  "dosage_form": "Dose 1",
  "indication": "Indication 4",
  "notify": "Yes",
  "state": "State 3",
  "category": "No",
  "dosage_frequency": "frequenst text",
  "duration": "20",
  "duration_unit": "Day(s)",
  "trade_name": "some trade name",
  "sponsor": "Some supplier name",
  "supply_date": "2020-03-19",
  "diagnosis": "diagnosis test",
  "clinical_justification": "clinical justification text",
  "intended_monitoring": "monitoring details",
  "prescriber_qualification": "prescriber qualification MD",
  "created_at": "2020-03-17 04:55:32",
  "updated_at": "2020-03-17 04:55:32"
  }
  ];

export interface IListGhostingExampleProps {
  items?: IExampleItem[];
}

export interface IListBasicExampleProps {
  items?: IExampleItem[];
}

export interface IListBasicExampleState {
  filterText?: string;
  items?: IExampleItem[];
}

interface IListBasicExampleClassObject {
  itemCell: string;
  itemImage: string;
  itemContent: string;
  itemName: string;
  itemIndex: string;
  chevron: string;
}

const theme: ITheme = getTheme();
const { palette, semanticColors, fonts } = theme;

const content = 'Today';

const stackTokens: IStackTokens = { childrenGap: 12 };

const HorizontalSeparatorStack = (props: { children: JSX.Element[] }) => (
  <>
    {React.Children.map(props.children, child => {
      return <Stack tokens={stackTokens}>{child}</Stack>;
    })}
  </>
);

const VerticalSeparatorStack = (props: { children: JSX.Element[] }) => (
  <Stack horizontal horizontalAlign="space-evenly">
    {React.Children.map(props.children, child => {
      return (
        <Stack horizontalAlign="center" tokens={stackTokens}>
          {child}
        </Stack>
      );
    })}
  </Stack>
);

const verticalStyle = mergeStyles({
  height: '200px'
});


interface IListGhostingExampleClassObject {
  container: string;
  itemCell: string;
  itemImage: string;
  itemContent: string;
  itemName: string;
  itemIndex: string;
  chevron: string;
}


const classNames: IListGhostingExampleClassObject = mergeStyleSets({
  container: {
    overflow: 'auto',
    maxHeight: 500
  },
  itemCell: [
    getFocusStyle(theme, { inset: -1 }),
    {
      minHeight: 54,
      padding: 10,
      boxSizing: 'border-box',
      borderBottom: `1px solid ${semanticColors.bodyDivider}`,
      display: 'flex',
      selectors: {
        '&:hover': { background: palette.neutralLight }
      }
    }
  ],
  itemImage: {
    flexShrink: 0
  },
  itemContent: {
    marginLeft: 10,
    overflow: 'hidden',
    flexGrow: 1
  },
  itemName: [
    fonts.xLarge,
    {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    }
  ],
  itemIndex: {
    fontSize: fonts.small.fontSize,
    color: palette.neutralTertiary,
    marginBottom: 10
  },
  chevron: {
    alignSelf: 'center',
    marginLeft: 10,
    color: palette.neutralTertiary,
    fontSize: fonts.large.fontSize,
    flexShrink: 0
  }
});

const MyIcon = () => <Icon iconName="TriangleSolidRight12" className="ms-IconExample" />;

export default class ListGhostingExample extends React.Component<IListGhostingExampleProps> {
  private _items: IExampleItem[];
  constructor(props: IListGhostingExampleProps) {
    super(props);
    this._items = [];
    const item1: IExampleItem = {
      thumbnail: "123",
      key: "",
      name: "Template name goes here",
      description: "",
      color: "white",
      shape: "1",
      location: "1",
      width: 1,
      height: 1,
    }
    const item2: IExampleItem = {
      thumbnail: "123",
      key: "",
      name: "Long template name goes here, Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ultrices accumsan dui.",
      description: "",
      color: "white",
      shape: "1",
      location: "1",
      width: 1,
      height: 1,
    }

    templates.forEach(element => {
      const item: IExampleItem = {
        thumbnail: "123",
        key: "",
        name: element.title,
        description: "",
        color: "white",
        shape: "1",
        location: "1",
        width: 1,
        height: 1,
      }
      this._items.push(item)
    });
  }

  public render(): JSX.Element {
    return (
          <div className="App">
      <HorizontalSeparatorStack>
      <header className="App-header">
        {/* <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a> */}
        <img src="./svg/logo_plugin.svg" />
        <Label
          styles={{
            root: {
              textAlign: "left",
              fontSize: "13px",

            }
          }}
          >Select template based on your patient's primary condition</Label>
        <SearchBox
          styles={{
            root : {width: "100%",}
          }}
          placeholder="Search"
          onSearch={newValue => console.log('value is ' + newValue)}
          onFocus={() => console.log('onFocus called')}
          onBlur={() => console.log('onBlur called')}
          onChange={() => console.log('onChange called')}
        />
      </header>
          <Separator />
          <div style={{
            padding: '10px',
          }}>
            <Label styles={{
              root: {
                textAlign: "left",
              }
            }}>Active template</Label>
          <Label disabled={true}>No template selected.</Label>
          </div>
            <Stack tokens={{ childrenGap: 20 }}>
        <Stack tokens={stackTokens} styles={{
          root: {
            padding: "10px",
            background: '#1276ca',
          }
        }}>
        <FocusZone direction={FocusZoneDirection.vertical}>
        <div className={classNames.container} data-is-scrollable={true}>
          <List items={this._items} onRenderCell={this._onRenderCell} />
        </div>
      </FocusZone>
      </Stack>
      </Stack>
        </HorizontalSeparatorStack>
    </div>
    );
  }

  private _onRenderCell(item: IExampleItem, index: number, isScrolling: boolean): JSX.Element {
    return (
      <div style={{
        background: 'white',
        borderRadius: '4px',
        padding: 0,
        minHeight: '30px',
        marginBottom: '6px',
      }} className={classNames.itemCell} data-is-focusable={true}>
        {/* <Image
          className={classNames.itemImage}
          src={isScrolling ? undefined : item.thumbnail}
          width={50}
          height={50}
          imageFit={ImageFit.cover}
        /> */}
        <div className={classNames.itemContent}>
          <Label styles={{root: {fontSize: "14px", whiteSpace: 'unset', paddingRight: '30px', textAlign: 'left',}}} className={classNames.itemName}>{item.name}</Label>
          {/* <div className={classNames.itemIndex}>{`Item ${index}`}</div> */}
          <button style={{
            position: "absolute",
            right: '4px',
            top: 'calc(50% - 11px)',
            borderRadius: '50%',
            height: '23px',
            width: '23px',
          }}>
            <Icon styles={{root: {
              width: '25px',
              height: '23px',
              left: 0,
              position: 'absolute',
              top: 0,
              lineHeight: '21px',
            }}} iconName="TriangleSolidRight12" className="ms-IconExample" />
          </button>
        </div>
      </div>
    );
  }
}


// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         {/* <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a> */}
//         <img src="./svg/logo_plugin.svg" />
//         <p>Select template based on your patient's primary condition</p>
//         <Stack tokens={{ childrenGap: 20 }}>
//         <SearchBox
//           placeholder="Search"
//           onSearch={newValue => console.log('value is ' + newValue)}
//           onFocus={() => console.log('onFocus called')}
//           onBlur={() => console.log('onBlur called')}
//           onChange={() => console.log('onChange called')}
//         />
//         <SearchBox
//           placeholder="Search with no animation"
//           onSearch={newValue => console.log('value is ' + newValue)}
//           onFocus={() => console.log('onFocus called')}
//           onBlur={() => console.log('onBlur called')}
//           onChange={() => console.log('onChange called')}
//           disableAnimation
//         />
//         <Stack tokens={stackTokens}>
//         <HorizontalSeparatorStack>
//             <Separator />
//         </HorizontalSeparatorStack>
//         <Label>Active template</Label>
//         <Label disabled={true}>No template selected.</Label>

//       </Stack>
//       </Stack>
//       </header>
//     </div>
//   );
// }

// export default App;
